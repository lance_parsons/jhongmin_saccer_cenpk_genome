# CEN-PK Genome

Prepare CEN-PK genome annotation files for RNA-Seq

1.  Create conda environment
    ```
    conda env create -f environment.yml
    source activate jhongmin_saccer_cenpk_genome
    ```

2.  Download genome files from YeastGenome
    ```
    cd data
    wget "https://downloads.yeastgenome.org/sequence/strains/CEN.PK/CEN.PK2-1Ca/CEN.PK2-1Ca_SGD_2015_JRIV01000000/CEN.PK2-1Ca.README"
    wget "https://downloads.yeastgenome.org/sequence/strains/CEN.PK/CEN.PK2-1Ca/CEN.PK2-1Ca_SGD_2015_JRIV01000000/CEN.PK2-1Ca_JRIV01000000_SGD.gff.gz"
    wget "https://downloads.yeastgenome.org/sequence/strains/CEN.PK/CEN.PK2-1Ca/CEN.PK2-1Ca_SGD_2015_JRIV01000000/CEN.PK2-1Ca_JRIV01000000_SGD_cds.fsa.gz"
    wget "https://downloads.yeastgenome.org/sequence/strains/CEN.PK/CEN.PK2-1Ca/CEN.PK2-1Ca_SGD_2015_JRIV01000000/CEN.PK2-1Ca_JRIV01000000_SGD_pep.fsa.gz"
    wget "https://downloads.yeastgenome.org/sequence/strains/CEN.PK/CEN.PK2-1Ca/CEN.PK2-1Ca_SGD_2015_JRIV01000000/CEN.PK2-1Ca_SGD_2015_JRIV01000000.fsa.gz"
    ```
